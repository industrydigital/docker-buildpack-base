FROM ubuntu:bionic-20200219
MAINTAINER Rich Choy <rich.choy@industrydigital.io>

ENV DEBIAN_FRONTEND=noninteractive
ENV RUNTIME_USER=runtime
ENV RUNTIME_GROUP=runtime
ENV RUNTIME_HOME=/home/runtime
ENV PROJECT_ROOT=/project

COPY ./assets /tmp/assets

RUN set -xeu \
# install apt dependencies
    && apt-get update \
    && apt-get install -y --no-install-recommends \
        apt-transport-https \
        build-essential \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common \
# create non-privileged runtime user and group
    && chmod 755 /tmp \
    && mkdir -p $RUNTIME_HOME \
    && groupadd --gid 1000 $RUNTIME_GROUP \
    && useradd --uid 1000 --home-dir $RUNTIME_HOME -g $RUNTIME_GROUP $RUNTIME_USER \
    && chown -R $RUNTIME_USER:$RUNTIME_GROUP $RUNTIME_HOME \
    && mkdir $PROJECT_ROOT \
    && echo "You need to mount this directory." > $PROJECT_ROOT/README.md \
    && chown -R $RUNTIME_USER:$RUNTIME_GROUP $PROJECT_ROOT \
# setup entrypoint
    && cp /tmp/assets/docker-entrypoint.sh /docker-entrypoint.sh \
    && chmod 755 /docker-entrypoint.sh \
# cleanup
    && apt-get clean -y \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/*

ENTRYPOINT ["/docker-entrypoint.sh"]
WORKDIR $PROJECT_ROOT
VOLUME [$PROJECT_ROOT]

