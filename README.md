# buildpack-base

## Features 

- Unprivileged "runtime" user that has a home directory at /home/runtime
- Assigns DOCKER_HOST_UID and DOCKER_HOST_GID to the run user inside the docker entrypoint
- Entrypoint executes command as run user

## Run user

```
runtime:runtime
```

## Environment variables

```
DOCKER_HOST_UID - uid to assign to the run user
DOCKER_HOST_GID - gid to assign to the run user
```

## Volumes

```
/project                 mount your project here
```
